/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tyorikan.voicerecordingvisualizer.sample;

import com.tyorikan.voicerecordingvisualizer.RecordingSampler;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ExampleOhosTest class
 **/
public class ExampleOhosTest {
    /**
     * testBundleName
     **/
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.tyorikan.voicerecordingvisualizer.sample", actualBundleName);
    }

    /**
     * testStartRecording
     **/
    @Test
    public void testStartRecording() {
        RecordingSampler mRecordingSampler = new RecordingSampler();
        assertEquals(false, mRecordingSampler.isRecording());
        mRecordingSampler.startRecording();
        assertEquals(true, mRecordingSampler.isRecording());
    }

    /**
     * testStartRecording
     **/
    @Test
    public void testStopRecording() {
        RecordingSampler mRecordingSampler = new RecordingSampler();
        assertEquals(false, mRecordingSampler.isRecording());
        mRecordingSampler.startRecording();
        assertEquals(true, mRecordingSampler.isRecording());
        mRecordingSampler.stopRecording();
        assertEquals(false, mRecordingSampler.isRecording());
        mRecordingSampler.release();
    }
}