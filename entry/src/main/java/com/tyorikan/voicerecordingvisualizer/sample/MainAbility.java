/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tyorikan.voicerecordingvisualizer.sample;

import com.tyorikan.voicerecordingvisualizer.sample.slice.MainAbilitySlice;
import com.tyorikan.voicerecordingvisualizer.sample.util.LogUtil;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import ohos.bundle.IBundleManager;

/**
 * MainAbility class
 */
public class MainAbility extends Ability {
    private static final String TAG = MainAbility.class.getSimpleName();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        if (verifySelfPermission("ohos.permission.MICROPHONE") != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(
                    new String[]{"ohos.permission.MICROPHONE"}, 1);
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
            LogUtil.info(TAG, "Permission Granted");
        }
    }
}
