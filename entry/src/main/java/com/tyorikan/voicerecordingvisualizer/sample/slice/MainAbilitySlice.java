/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tyorikan.voicerecordingvisualizer.sample.slice;

import com.tyorikan.voicerecordingvisualizer.RecordingSampler;
import com.tyorikan.voicerecordingvisualizer.VisualizerView;
import com.tyorikan.voicerecordingvisualizer.sample.MainAbility;
import com.tyorikan.voicerecordingvisualizer.sample.ResourceTable;

import com.tyorikan.voicerecordingvisualizer.sample.util.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;

/**
 * MainAbilitySlice class
 */
public class MainAbilitySlice extends AbilitySlice implements RecordingSampler.CalculateVolumeListener  {
    private static final String TAG = MainAbilitySlice.class.getSimpleName();
    boolean hasMicrophonePermission = false;

    private Button mFloatingActionButton;

    // Recording Info
    private RecordingSampler mRecordingSampler;

    // View
    private VisualizerView mVisualizerView;
    private VisualizerView mVisualizerView2;
    private VisualizerView mVisualizerView3;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        {
            mVisualizerView = (VisualizerView) findComponentById(ResourceTable.Id_visualizer);
            ComponentTreeObserver observer = mVisualizerView.getComponentTreeObserver();
            observer.addGlobalFocusUpdatedListener(new ComponentTreeObserver.GlobalFocusUpdatedListener() {
                @Override
                public void onGlobalFocusUpdated(Component component, Component component1) {
                    mVisualizerView.setBaseY(mVisualizerView.getHeight());
                    mVisualizerView.getComponentTreeObserver().removeGlobalFocusUpdatedListener(this);
                }
            });
        }

        {
            mVisualizerView2 = (VisualizerView) findComponentById(ResourceTable.Id_visualizer2);
            ComponentTreeObserver observer = mVisualizerView2.getComponentTreeObserver();
            observer.addGlobalFocusUpdatedListener(new ComponentTreeObserver.GlobalFocusUpdatedListener() {
                @Override
                public void onGlobalFocusUpdated(Component component, Component component1) {
                    mVisualizerView2.setBaseY(mVisualizerView2.getHeight() / 5);
                    mVisualizerView2.getComponentTreeObserver().removeGlobalFocusUpdatedListener(this);
                }
            });
        }

        mVisualizerView3 = (VisualizerView) findComponentById(ResourceTable.Id_visualizer3);
        mFloatingActionButton = (Button) findComponentById(ResourceTable.Id_fab);

        hasMicrophonePermission =
                verifySelfPermission("ohos.permission.MICROPHONE") == IBundleManager.PERMISSION_GRANTED;

        // create AudioRecord
        mRecordingSampler = new RecordingSampler();
        mRecordingSampler.setVolumeListener(this);
        mRecordingSampler.setSamplingInterval(100);
        mRecordingSampler.link(mVisualizerView);
        mRecordingSampler.link(mVisualizerView2);
        mRecordingSampler.link(mVisualizerView3);

        mFloatingActionButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                performClick();
            }
        });
    }

    private void performClick() {
        if (hasMicrophonePermission) {
            if (mRecordingSampler.isRecording()) {
                mFloatingActionButton.setText("Turn On Mic");
                mRecordingSampler.stopRecording();
            } else {
                mFloatingActionButton.setText("Turn Off Mic");
                mRecordingSampler.startRecording();
            }
        } else {
            new ToastDialog(getContext())
                    .setText("App does not have Microphone permission")
                    .show();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onCalculateVolume(int volume) {
        LogUtil.info(TAG, "onCalculateVolume volume = " + volume);
    }

    @Override
    protected void onInactive() {
        if (hasMicrophonePermission) {
            if (mRecordingSampler.isRecording()) {
                mFloatingActionButton.setText("Turn On Mic");
                mRecordingSampler.stopRecording();
            }
            super.onInactive();
        }
    }

    @Override
    protected void onStop() {
        mRecordingSampler.release();
        super.onStop();
    }
}
