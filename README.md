# voice-recording-visualizer

voice-recording-visualizer : Simple Visualizer from mic input for HMOS.

# Usage Instructions

Add the `com.tyorikan.voicerecordingvisualizer.VisualizerView to your layout XML file with the following values.
app:numColumns -> now of columns that is required in visualizer
app:renderColor -> color of the bars to be displayed in visualizer, default color is White
app:renderRange -> represents the renderRange "top", "bottom", "both", for any other value or if it not mentioned default value is "top"
app:renderType -> represents the renderType "bar", "pixel", for any other value or if it not mentioned default value is "bar"
app:fade -> represents the fade property and it should have a value "fade", for any other value or if it not mentioned fade property is not added

```
    <com.tyorikan.voicerecordingvisualizer.VisualizerView
            ohos:id="$+id:visualizer2"
            ohos:width="50vp"
            ohos:height="150vp"
            ohos:layout_alignment="horizontal_center|top"
            ohos:top_margin="$integer:spacing_large"
            ohos:background_element="$graphic:background_white"
            app:numColumns="10"
            app:renderColor="$color:renderColor2"
            app:renderRange="bottom"
            app:renderType="pixel"
            app:fade="fade"/>
```
Add required VisualizerView to Layout

```
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ...
        VisualizerView visualizerView = (VisualizerView) findViewById(R.id.visualizer);

        RecordingSampler recordingSampler = new RecordingSampler();
        recordingSampler.setVolumeListener(this);  // for custom implements
        recordingSampler.setSamplingInterval(100); // voice sampling interval
        recordingSampler.link(visualizerView);     // link to visualizer

        recordingSampler.startRecording(); //recordingSampler.stopRecording(); to stop recording
    }

    @Override
    protected void onInactive() {
        if (hasMicrophonePermission) {
            if (mRecordingSampler.isRecording()) {
                mFloatingActionButton.setText("Turn On Mic");
                mRecordingSampler.stopRecording();
            }
            super.onInactive();
        }
    }

    @Override
    protected void onStop() {
        mRecordingSampler.release();
        super.onStop();
    }
```

# Installation Instructions
1.For using voice-recording-visualizer module in sample application,include the below library dependency to generate hap/voicerecordingvisualizer.har.

Modify entry build.gradle as below :
```
dependencies {
    implementation project(path: ':visualizer')
}
```

2.For using voice-recording-visualizer in separate application, make sure to add the "voicerecordingvisualizer.har" in libs folder of "entry" module.

Modify entry build.gradle as below :
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```

3.For using voice-recording-visualizer from the remote repository, add the below dependency in "entry" build.gradle.

    Modify entry build.gradle as below :
    dependencies {
        implementation 'io.openharmony.tpc.thirdlib:voice-recording-visualizer:1.0.0'
    }

## License
    Copyright 2015 tyorikan

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.